class Led {
    private byte DDRB;
    private byte PORTB;

    Led() {
        DDRB = 0;
        PORTB = 0;
    }

    void initLed(int led) {
        DDRB |= (1<<led);
    }

    void setLedOn(int led) {
        PORTB |= (1<<led);
    }

    void setLedOff(int led) {
        PORTB &= ~(1<<led);
    }

    String getDDRP() {
        return Integer.toBinaryString(DDRB);
    }

    String getPORTB() {
        return Integer.toBinaryString(PORTB);
    }

    void reset() {
        DDRB = 0;
        PORTB = 0;
    }

}