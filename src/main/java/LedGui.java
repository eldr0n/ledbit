import com.formdev.flatlaf.FlatDarculaLaf;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;


public class LedGui {

    private Led led;
    private JRadioButton led3;
    private JRadioButton led2;
    private JRadioButton led1;
    private JRadioButton led0;
    private JLabel ddrb;
    private JLabel portb;
    private JTextField f1;
    private JTextField f2;

    private LedGui() {
        led = new Led();
        createWindow();
    }

    private void createWindow() {

        JFrame window = new JFrame("LedGUI");
        Container contentPane = window.getContentPane();

        contentPane.setLayout(new BorderLayout());

        contentPane.add(createNorthLayout(), BorderLayout.NORTH);
        contentPane.add(createCenterLayout(), BorderLayout.CENTER);
        contentPane.add(createSouthLayout(), BorderLayout.SOUTH);

        window.pack();
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JPanel createNorthLayout() {
        //ImageIcon off = new ImageIcon("off.png");
        JPanel panelN = new JPanel();
        panelN.setBorder(new EtchedBorder());
        panelN.setLayout(new GridLayout(1, 4));
        led3 = new JRadioButton("LED 3", false);
        panelN.add(led3);
        led2 = new JRadioButton("LED 2", false);
        panelN.add(led2);
        led1 = new JRadioButton("LED 1", false);
        panelN.add(led1);
        led0 = new JRadioButton("LED 0", false);
        panelN.add(led0);

        return panelN;
    }

    private JPanel createCenterLayout() {
        JPanel panelC = new JPanel();
        panelC.setBorder(new EtchedBorder());
        panelC.setLayout(new GridLayout(2, 2));

        panelC.add(new JLabel("DDRB:"));
        ddrb = new JLabel(led.getDDRP(), SwingConstants.RIGHT);
        panelC.add(ddrb);

        panelC.add(new JLabel("PORTB:"));
        portb = new JLabel(led.getPORTB(), SwingConstants.RIGHT);
        panelC.add(portb);


        return panelC;

    }

    private JPanel createSouthLayout() {
        JPanel panelS = new JPanel();
        panelS.setBorder(new EtchedBorder());
        panelS.setLayout(new GridLayout(4, 3));

        panelS.add(new JLabel("init LED"));
        f1 = new JTextField("");
        JButton b1 = new JButton("init");
        panelS.add(f1);
        panelS.add(b1);
        b1.addActionListener(e -> {
            String valueF1 = f1.getText();
            led.initLed(Integer.parseInt(valueF1));
            f1.setText("");
            ddrb.setText(led.getDDRP());
        });

        panelS.add(new JLabel("LED on"));
        f2 = new JTextField("");
        JButton b2 = new JButton("set");
        panelS.add(f2);
        panelS.add(b2);
        b2.addActionListener(e -> {
            String valueF2 = f2.getText();
            led.setLedOn(Integer.parseInt(valueF2));
            ledOn(valueF2);
            f2.setText("");
            portb.setText(led.getPORTB());
        });

        panelS.add(new JLabel("LED off"));
        JTextField f3 = new JTextField("");
        JButton b3 = new JButton("set");
        panelS.add(f3);
        panelS.add(b3);
        b3.addActionListener(e -> {
            String valueF3 = f3.getText();
            led.setLedOff(Integer.parseInt(valueF3));
            ledOff(valueF3);
            f3.setText("");
            portb.setText(led.getPORTB());
        });


        JButton bR = new JButton("reset");
        panelS.add(bR);
        bR.addActionListener(e -> {
            led.reset();
            ddrb.setText(led.getDDRP());
            portb.setText(led.getPORTB());
            led0.setSelected(false);
            led1.setSelected(false);
            led2.setSelected(false);
            led3.setSelected(false);
        });


        return panelS;
    }

    private void ledOn(String value) {
        switch (value) {
            case "0":
                led0.setSelected(true);
                break;
            case "1":
                led1.setSelected(true);
                break;
            case "2":
                led2.setSelected(true);
                break;
            case "3":
                led3.setSelected(true);
                break;
        }
    }

    private void ledOff(String value) {
        switch (value) {
            case "0":
                led0.setSelected(false);
                break;
            case "1":
                led1.setSelected(false);
                break;
            case "2":
                led2.setSelected(false);
                break;
            case "3":
                led3.setSelected(false);
                break;
        }
    }

    public static void main(String[] args) {
        FlatDarculaLaf.install();
        new LedGui();
    }
}